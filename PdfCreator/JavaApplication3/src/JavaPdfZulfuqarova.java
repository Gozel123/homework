import static com.itextpdf.text.Annotation.URL;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfCell; 
import static com.itextpdf.text.pdf.PdfName.URL;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTable;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaPdfZulfuqarova{
    public static void main(String[]args) throws DocumentException, FileNotFoundException, MalformedURLException{
  Document document =new Document();
  {try{
      PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("FirstExam.pdf"));
      document.open();
      document.add(new Paragraph("It's first task from you.Iwant to add image and table"));
      String imageUrl="http://cdn.wonderfulengineering.com/wp-content/uploads/2014/04/code-wallpaper.jpg";
      Image image1=Image.getInstance(new URL(imageUrl));
      image1.setAbsolutePosition(10f,10f);
    image1.scaleAbsolute(600,800);
      document.add(image1);
      
      PdfPTable table =new PdfPTable(2);
      table.setWidthPercentage(100);
      table.setSpacingBefore(10f);
      table.setSpacingAfter(10f);
      
      float[] columnWidths={1f,1f};
      table.setWidths(columnWidths);
      PdfPCell cell1 = new PdfPCell(new Paragraph("Eng23"));
        cell1.setBorderColor(BaseColor.BLUE);
        cell1.setPaddingLeft(10);
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
 
        PdfPCell cell2 = new PdfPCell(new Paragraph("ZULFUQAROVA_GOZEL"));
        cell2.setBorderColor(BaseColor.RED);
        cell2.setPaddingLeft(10);
        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(cell1);
        table.addCell(cell2);
        document.add(table);
 
        document.close();
        writer.close();
      

    } catch (BadElementException | IOException ex) {
          Logger.getLogger(JavaPdfZulfuqarova.class.getName()).log(Level.SEVERE, null, ex);
      }}}
}



